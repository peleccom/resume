Alexander Pitkin
============
----

>  Python/Django developer.

>  <pitkinalexander@gmail.com>
>  +375 33 3242335

----

Education
---------

2015-2017 (master)
:   **Master of informatics and and computing, The chair electronic computing machines**;  Belarusian state university of informatics and radioelectronics

    *Minsk, Belarus*

2010-2015 (bachelor)
:   **Systems engineer, The chair electronic computing machines**;  Belarusian state university of informatics and radioelectronics

     *Minsk, Belarus*

2006-2010
:   **Technician on telecommunications, Software telecommunications networks**; The Highest State College of Communications, Vitebsk branch

    *Vitebsk, Belarus*

Skills
---------

**Language/Programing**:

:	Python
:   Bash
:   C/C++/C#/Java/Delphi
:	Javascript/TypeScript
:	CSS (LESS/SASS/Stylus)
:	HTML

**Frameworks/Techologies**:
:	Django/Django Rest Framework
:	FastAPI
:	Flask
:	SQLAlchemy/GeoAlchemy 2
:   AWS/GCP
:   Docker
:   Kubernetes
:	NodeJS
:	AngularJs/Angular 2+
:	RxJS
:   CircleCI
:	React Native/Android SDK

**Systems**:
:	Linux
:	Windows


**Databases**:

:	SQL
:	Postgres
:	MSSQL
:	MySQL
:	Redis
:	Memcached
:	ElasticSearch


**Project/Task Management Tools**:

:	Asana
:	Trello
:	JIRA


Experience (10+ years)
----------
**2017 - present iTechArt, Software Engineer**

Senior fullstack developer

**2014 - 2017	Nord-Soft, Python/Django developer**

Creating and maintenance web sites using django. Creating web site API using *Django Rest Framework*, Database development
Working with frontend (Javascript, JQuery, AngularJS/Angular2+, CSS/LESS)

Projects
----------
1.  Health and Wellness Assistant application
    * *Stack*: Python, SQLAlchemy, AWS lambda, AWS, Alembic, pytest, Docker
    * *Role*: Senior Software Engineer
    * *Responsibilities*:
        * Develop API
        * Test implementation
        * Implement schedule service

1. Real Estate Portal API microservices - various API microservices for real estate portal
    * *Stack*: Django, Flask, FastAPI, SQLAlchemy, Postgres, Postgis, Redis, Docker, Kubernetes
    * *Role*: Senior Software Engineer
    * *Responsibilities*:
        * Implement API microservice
        * Split monolitic application into microservice
        * Develop backend packages to reuse in different microservices
        * Create complicated geospatial queries to fetch required data
        * Develop backend packages to reuse in different microservices
        * Setup service integration

1. Leading global eGift card platform with operations in Australia, North America, United Kingdom and New Zealand. Project aims to simplify and enhance the gift-giving experience by allowing users to purchase, send, and receive digital gift cards from a wide range of popular retailers and brands.
    * *Stack*: Django, FastAPI, AWS, AWS Lambda, Celery, Postgres, pytest, Graphene-Django, SOAP, Stripe
    * *Role*: Senior Software Engineer
    * *Responsibilities*:
        * Build an API integration between project core and different 3rd party giftcards providers.
        * Develop the microservice to improve site performance and isolate API integration logic
        * Write a highly testable code with different test cases and live tests which check the health of external services</>
        * Constant communication with gift providers techical experts create integration with private APIs

1.  Real Estate Portal printing service - Project Web service to make pdf for a given URL
description
    * *Stack*: NodeJS, Express, Puppeteer, bull
    * *Role*: Senior Software Engineer
    * *Responsibilities*:
        * Developed express API
        * Developed Puppeteer integration
        * Developed Docker and CircleCI integration
        * Added API tests and compare PDF tests

1. Driving licence exam booking system.
    * *Stack*: Django, React.js, Celery, PostgreSQL, Redis, Docker, Traefik
    * *Role*: Senior Software Engineer
    * *Achievements*:
        * Developed the back-end
        * Designed the data model and implemented the database schema
        * Implemented the deployment part and deployed the application to server
        * Added the Celery application to run periodical time slots booking
        * Optimized application performance

1. Real Estate Portal: Data Intelligence Board - A Web application that allows users to: - obtain price valuations and prediction for the real estate property - understand the market - explore the surroundings (location & nearby)
    * *Stack*: AngularJs, Angular, Django, Flask, SQLAlchemy, HTML 5, CSS3, Alembic, Docker,
Mapbox, Google Maps API, PostGIS, PostgreSQL
    * *Role*: Software Engineer
    * *Responsibilities*
        * Implemented a new version of the Web API with Flask and SQLAlchemy
        * Reworked big subsets of the data model and updated the database schema accordingly
        * Developed and implemented the front-end application
        * Migrated a big live application from AngularJS to Angular2
        * Developed set of custom reusable data charts using d3.js library
        * Developed a various of map integration with user interaction support
        * Integrated the app with other services


1. Web portal for management students housing. The portal handles the entire student leasing experience and provides easy tools to help managers track assignments, payments, and communication
    * *Role*: Software Engineer
    * *Back-end development*:
        * Models design
        * Logic/architecture design
        * Django views
        * REST API for 3rd partly applications, mobile and front-end
        * Payment system integration
        * DB and site loading optimization

    * *Front-end development*:
        * CSS/HTML
        * Angular templates development
        * Back-end Integration
    * *Main technologies and products used*: Python/Django, Celery, AngularJS, HTML/CSS, JavaScript/Coffescript, jQuery, OAuth, PotgreSQL, Stripe API


1. User generated news portal. Allow user to rate news and change public news ratings
    * *Role*: Software Engineer
    * *Back-end development*:
        * Models design;
        * Logic/architecture design
        * Django views.
        * REST API for front-end
        * Celery tasks
    * *Front-end development*:
        * CSS/HTML
        * Angular templates development
        * Back-end Integration
    * *Main technologies and products used*: Python/Django, AngularJS ,Celery, HTML/CSS, JavaScript, jQuery, jQuery.ui, PotgreSQL, Redis

1. Person Heath Assistance – user personal health assistance application. Application encourage healthy user habits.
    * *Role*: Software Engineer
    * *Back-end development* :
        * database development and maintenance;
        * backend and frontend implementation- add new features;
        * bug fixes;
        * DB and site loading optimization;
        * implementing Django templates;
    * *Main technologies and products used*: Python/Django, HTML/CSS, JavaScript, jQuery, jQuery.ui, Backbone, Celery, MSSQL Server, Google Cloud Messaging




Languages
--------------------

**Russian** native

**English** upper-intermediate


Cerificates
--------------------

**Cisco Certified Network Associate(CCNA Routing and Switching (v5.0))**, 2015
Cisco Networking academy


**Linux development courses**, 2014
EPAM


**Android Development training**, 2013
ForteKnowledge


**Cisco Certified Network Associate (CCNA1, CCNA2)**, 2008-2009
Cisco Networking academy — Toronto, CANADA


**Programming in python**, 2011
INTUIT NATIONAL OPEN UNIVERSITY
[http://www.intuit.ru/verifydiplomas/00119534](http://www.intuit.ru/verifydiplomas/00119534)


Others
--------------------

**My Github profile**
[https://github.com/peleccom](https://github.com/peleccom)

**My LinkeIn profile**
[https://www.linkedin.com/pub/alexander-pitkin/57/1a4/9b2](https://www.linkedin.com/pub/alexander-pitkin/57/1a4/9b2)


----

> <pitkinalexander@gmail.com> • +375 33 324 23 35 • 34 years old\
> Minsk, Belarus. 2024
