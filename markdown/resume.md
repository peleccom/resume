Alexander Pitkin
============
----

>  Fullstack Python/Django developer.

>  <pitkinalexander@gmail.com>
>  +375 33 3242335

----

Education
---------

2015-2017 (master)
:   **Master of informatics and computing, The chair electronic computing machines**;  Belarusian state university of informatics and radioelectronics

    *Minsk, Belarus*

2010-2015 (bachelor)
:   **Systems engineer, The chair electronic computing machines**;  Belarusian state university of informatics and radioelectronics

     *Minsk, Belarus*

2006-2010
:   **Technician on telecommunications, Software telecommunications networks**; The Highest State College of Communications, Vitebsk branch

    *Vitebsk, Belarus*

Skills
---------

**Language/Programming**:

:	Python
:	Javascript/TypeScript
:   Bash
:	CSS (LESS/SASS/Stylus)
:	HTML
:   C/C++/C#/Java/Delphi/Dart

**Frameworks/Techologies**:
:	Django/Django Rest Framework
:	AngularJs/Angular 2+
:   React
:	FastAPI
:	Flask/Connexion
:	SQLAlchemy/GeoAlchemy 2/Alembic
:	NodeJS
:	RxJS
:	React Native/Android SDK/Flutter

**Systems**:
:	Linux
:	Windows

**Cloud & CI/CD**:
:	AWS/Google cloud Platform
:	CircleCI/Gitlab
:	Docker/Kubernetes

**Databases/Message brokers**:

:	SQL
:	Postgres
:	MSSQL
:	MySQL
:	Redis
:	RabbitMQ
:	Memcached
:	ElasticSearch


**Project/Task Management Tools**:

:	JIRA
:	Asana
:	Trello


Experience (10+ years)
----------
**2017 - present iTechArt, Software Engineer**

Senior fullstack developer. Developed scalable API services for high-traffic platforms, including microservices architecture. Developed integration with external API solutions.

**2014 - 2017	Nord-Soft, Python/Django developer**

Python Developer. Developed and maintained robust web applications and APIs using Django and Django REST Framework. Developing dynamic front-end features with AngularJS and JavaScript/Jquery. Optimizing database and sites performance.

Projects
----------

1.  Health and Wellness Assistant application
    * *Tech Stack*: Python, SQLAlchemy, AWS lambda, AWS services, DynamoDB, Amazon Rekognition, Alembic, pytest, Docker
    * *Role*: Senior Software Engineer
    * *Responsibilities*:
        * Developed serverless API based on AWS lambdas
        * Designed and developed system architecture
        * Improved test setup and test performance
        * Improved test coverage with unit and integration tests
        * Delivered and automated schedule service

1. Real Estate Portal API microservices - various API microservices for real estate portal
    * *Tech Stack*: Django, Flask, FastAPI, SQLAlchemy, Postgres, Postgis, Redis, Docker, Kubernetes
    * *Role*: Senior Software Engineer
    * *Responsibilities*:
        * Implemented API microservice
        * Acted as the Technical Lead, managing team workloads, setting coding standards, overseeing technical tasks
        * Split monolithic application into microservices
        * Developed backend packages and modules to reuse in different microservices
        * Created complex geospatial SQL queries for advanced location-based search capabilities
        * Setup service integration

1.  Real Estate Portal printing service - Project Web service to make pdf for a given URL
description
    * *Tech Stack*: NodeJS, Express, Puppeteer, bull
    * *Role*: Senior Software Engineer
    * *Responsibilities*:
        * Developed a high-volume PDF generation service based on NodeJS, Puppeteer and message queue
        * Setup Docker and CircleCI integration
        * Improved quality assurance with automated test tool for PDF comparison

1. Leading global eGift card platform with operations in Australia, North America, United Kingdom and New Zealand. Project aims to simplify and enhance the gift-giving experience by allowing users to purchase, send, and receive digital gift cards from a wide range of popular retailers and brands.
    * *Tech Stack*: Django, FastAPI, AWS, AWS Lambda, Celery, Postgres, pytest, Graphene-Django, SOAP, Stripe
    * *Role*: Senior Software Engineer
    * *Responsibilities*:
        * Created seamless API integrations between the platform’s core system and multiple third-party gift card providers, ensuring reliability and scalability.
        * Developed the microservice to improve site performance and isolate API integration logic
        * Delivered high-quality, testable code with comprehensive test coverage, including live health checks for external service integrations.
        * Collaborated with technical experts from gift card providers to resolve critical issues, ensuring robust and smooth integration with proprietary APIs.

1. Real Estate Portal: Data Intelligence Board - A Web application that allows users to: - obtain price valuations and prediction for the real estate property - understand the market - explore the surroundings (location & nearby)
    * *Tech Stack*: AngularJs, Angular, React, Django, Flask, SQLAlchemy, HTML 5, CSS3, D3.js, Alembic, Docker,
Mapbox, Google Maps API, PostGIS, PostgreSQL
    * *Role*: Software Engineer
    * *Responsibilities*
        * Implemented a new version of the Web API with Flask and SQLAlchemy
        * Refactored large subsets of the data model and optimized the database schema to improve performance
        * Developed and implemented the front-end application with advanced user integrations
        * Seamlessly migrated a big live application from AngularJS to Angular2
        * Created a suite of reusable, interactive data visualization charts using the D3.js library
        * Developed map integration with user interaction features, using map tools like MapBox and Google Maps API
        * Integrated the app with other services and provided embedded widgets

1. Driving licence exam booking system.
    * *Tech Stack*: Django, React.js, Celery, PostgreSQL, Redis, Docker, Traefik
    * *Role*: Senior Software Engineer
    * *Achievements*:
        * Architected and Developed the back-end
        * Designed an optimized data model and implemented the database schema
        * Automated deployment processes and successfully deployed the application to production servers
        * Integrated Celery to enable periodic and automated time-slot booking
        * Optimized application performance

1. Web portal for management students housing. The portal handles the entire student leasing experience and provides easy tools to help managers track assignments, payments, and communication
    * *Tech Stack*: Django, Django Rest Framework, Celery, PostgreSQL, AngularJS, HTML/CSS, JavaScript/Coffescript, Redis, OAuth, Stripe
    * *Role*: Software Engineer
    * *Achievements*:
        * Created database models design
        * Designed and implemented scalable application logic and architecture
        * Created efficient Django views to handle core backend processes
        * Developed a REST API for seamless integration with third-party applications, mobile clients, and the front-end
        * Integrated a secure and reliable payment system to manage transactions
        * Optimized database queries and application performance to ensure swift and reliable service under high load
        * Developed AngularJS frontend application

1. User generated news portal. Allow user to rate news and change public news ratings
    * *Tech Stack*: Python/Django, AngularJS ,Celery, HTML/CSS, JavaScript, jQuery, jQuery.ui, PotgreSQL, Redis
    * *Role*: Software Engineer
    * *Achievements*:
        * Designed data models to support dynamic content and user interactions
        * Architected and implemented business logic to ensure efficient data flow
        * Developed Django views to handle user requests and data processing
        * Built RESTful API for seamless communication with the front-end
        * Implemented background tasks using Celery for scheduled news rating updates
        * Created responsive and modern layouts using CSS/HTML
        * Developed dynamic AngularJS templates to ensure smooth user interactions
        * Integrated front-end with back-end systems


1. Person Heath Assistance – user personal health assistance application. Application encourage healthy user habits.
    * *Tech Stack*: Python/Django, HTML/CSS, JavaScript, jQuery, jQuery.ui, Backbone, Celery, MSSQL Server, Google Cloud Messaging
    * *Role*: Software Engineer
    * *Achievements* :
        * Designed and maintained the application database to ensure efficient data storage and retrieval
        * Implemented and integrated back-end and front-end features to enhance user experience and functionality
        * Addressed and resolved bugs and critical issues
        * Optimized database performance and site load times
        * Developed and integrated Django templates





Languages
--------------------

**Russian** native

**English** upper-intermediate (B2)


Cerificates
--------------------

**Cisco Certified Network Associate(CCNA Routing and Switching (v5.0))**, 2015
Cisco Networking academy


**Linux development courses**, 2014
EPAM


**Android Development training**, 2013
ForteKnowledge


**Cisco Certified Network Associate (CCNA1, CCNA2)**, 2008-2009
Cisco Networking academy — Toronto, CANADA


**Programming in python**, 2011
INTUIT NATIONAL OPEN UNIVERSITY
[http://www.intuit.ru/verifydiplomas/00119534](http://www.intuit.ru/verifydiplomas/00119534)


Others
--------------------

**My Github profile**
[https://github.com/peleccom](https://github.com/peleccom)

**My LinkeIn profile**
[https://www.linkedin.com/pub/alexander-pitkin/57/1a4/9b2](https://www.linkedin.com/pub/alexander-pitkin/57/1a4/9b2)


----

> <pitkinalexander@gmail.com> • +375 33 324 23 35 • 35 years old\
> Minsk, Belarus. 2025
